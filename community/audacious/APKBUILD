# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=audacious
pkgver=4.0.1
pkgrel=0
pkgdesc="A playlist-oriented media player with multiple interfaces"
url="https://audacious-media-player.org/"
arch="all !s390x"
license="BSD-2-Clause AND ISC"
depends_dev="dbus-glib-dev qt5-qtbase-dev"
makedepends="$depends_dev libxml2-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-dbg"
source="http://distfiles.audacious-media-player.org/$pkgname-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-chardet \
		--disable-valgrind \
		--enable-dbus \
		--disable-gtk \
		--enable-qt
	make
}

check() {
	cd "$builddir"/src/libaudcore/tests
	make test
	./test
}

package() {
	DESTDIR="$pkgdir" make install
}

sha512sums="bd1387af281e7f533d4a21bf9fdb8ae49d5a406c051bcfa3a509b9d3cf8fdf6faff21dee841fad9ce1c9bf1a24ce1c08d9dc27a7a28b78ade3f0075fa89aad3d  audacious-4.0.1.tar.bz2"
